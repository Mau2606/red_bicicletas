var map = L.map('main_map').setView([-33.4378394,-70.6526683], 12);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);


//coordenadas san miguel -33.4976475,-70.671361
//L.marker([-33.4976475,-70.671361]).bindPopup('San Miguel').addTo(map);
//-33.4496939,-70.6778865 Santiago Centro
//L.marker([-33.4496939,-70.6778865]).bindPopup('Santiago Centro').addTo(map);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title:bici.id}).addTo(map);
        });
    }
})